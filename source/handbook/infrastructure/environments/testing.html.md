### Development

* Name: Development 
* URL:  various 
* Purpose: local development envrionment 
* Deploy: on save
* Database: Fixture 
* Terminal access: Developer

 Development happens on a local machine. Therefore there is no way to provide any SLA. 
 Access is to the individual dev. This could be either EE/CE depending on what the developer is working on.

### Demo

* Name: Demo 
* URL:  [spreadsheet](`https://docs.google.com/a/gitlab.com/spreadsheets/d/1HZ-7XhDNzdCBxfjzDFIQi7EjliptkpY4CB3LbiLa9MY/edit?usp=drive_web`)  
* Purpose: Sales 
* Deploy: Release
* Database: Fixture 
* Terminal access: Production Team

  This should be a fully featured version of the current EE release. 
  The high SLA and tightened access is to ensure it is always available for sales. 
  There are no features (feature flags/canary/etc) that we do not ship.

### Dev  

* Name: Dev 
* URL:  [dev.gitlab.org](http://dev.gitlab.org) 
* Purpose: Tools to build GitLab.com 
* Deploy: Nightly
* Database: Real Work 
* Terminal access: Production and build team 

  Currently there are two main uses for dev, Builds and repos that are needed in
  case gitlab.com is offline.  This is a critical piece of infrastructure that is 
  always growing in size due to build artifacts.  There  are discussions to make a 
  new build server where nightly CE/EE builds can be deployed or to move the infra 
  repos to a new host that would be an separate (not gitlab.com) EE instance.  

### Review Apps
  
* Name: Review App 
* URL:  various
* Purpose: Test proposal 
* Deploy: On commit
* Database: Fixtures
* Terminal access: Review app owner

 ephemeral app environments that are created dynamically every time you push a 
 new branch up to GitLab, and they&#39;re automatically deleted when the branch 
 is deleted. Single container with limited access.

### One-off 

* Name: one-off 
* URL:  various
* Purpose: Testing specific features in a large environment 
* Deploy: Release + patches
* Database: User Specific  
* Terminal access: Team developing feature 

  This is less of a staging environment more like a large scale development environments. 
  This could be because of the number of repos required, or a full sized db is required. 
  A version of CE/EE is installed and then patches are applied as work progresses. 
  These should be very limited in number.


### Staging

* Name: Staging  
* URL:  [staging.gitlab.org](http://staging.gitlab.org) 
* Purpose: To test master
* Deploy: Nightly
* Database: [Pseudonymizationof prod](https://en.wikipedia.org/wiki/Pseudonymization) 
* Terminal access: All engineers

  
**version 0.5**  

  This is the version we have today. A full environment that is managed by Chef 
  and Terraform(single environment).  This setup can at most be updated nightly\* 
  because it requires an omnibus version to install. This would be a static 
  environment with a pseudonymization production database.  the DB is a snapshot 
  of the pre-prod DB (updated only often enough to keep migration times to a minimum).

 \* or however often we can have an omnibus build created.

**Version 1.0**

K8s &amp; helm charts (cloud native)

   The final version of staging is multiple container deploys managed by K8s via 
   helm charts. This could be mapped to Master and re-deployed every time there 
   is a successful merge to master.  There is already work started to move us to 
   containers.   [https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2420](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2420)

### Pre-production

* Name: preprod 
* URL:  [preprod.gitlab.com](http://preprod.gitlab.com)
* Purpose: Simulate prod 
* Deploy: Release Canidate 
* Database: Weekly copy of Production   
* Terminal access: Production team 

 The pre-prod environment will be a mirror of the production environment topology, 
 a full production DB and access restrictions. We will utilize end to end tests , 
 via gitlab-qa, to ensure the quality of the deploy. In the future I would like 
 to be able to mirror production traffic into pre-prod. This would make all of 
 pre-prod in effect a canary deploy, except that if it breaks under load, real 
 users are not affected.

## Production 
* Name: Production 
* URL:  [www.gitlab.com](http://www.gitlab.com)
* Purpose: Production 
* Deploy: Release Canidate 
* Database: Production   
* Terminal access: Production team 

  Production will be full scale and size with the ability to have a canary deploy. 
   Production has limited access.  
   
## Self-Hosted
* Name: self-hosted 
* URL:  varous 
* Purpose: Self hosted versions of CE & EE  
* Deploy: User specific 
* Database: User specific   
* Terminal access: User specific 

 These are environments that are run on-premise by the end-user. We have no influence,
  access or control of these environments.