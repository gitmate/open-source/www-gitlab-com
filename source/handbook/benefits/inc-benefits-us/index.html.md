---
layout: markdown_page
title: "GitLab Inc (US) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Specific to US based employees
{: #us-specific-benefits}

US based employees' payroll and benefits are arranged through TriNet. The benefits decision discussions are held by People Operations, the CFO, and the CEO in the summer, to elect the next year's benefits by TriNet's deadline. People Operations will notify the team of open enrollment as soon as details become available.    

The most up to date and correct information is always available to employees through the {TriNet HRPassport portal](https://www.hrpassport.com) and the various contact forms and numbers listed there. This brief overview is not intended to replace the documentation in TriNet, but rather to give our GitLabbers and applicants a quick reference guide.

If you have any questions regarding benefits or TriNet's platform, feel free to call their customer solutions line at 800-638-0461. If you run into any issues with the platform itself, try switching from Chrome to Safari to login and enroll in benefits/view your profile. If you have any questions in regards to your TriNet paycheck log in to TriNet, then go to [How To Read Your Paycheck](https://www.hrpassport.com/Help/Docs/pdf/Readpaycheck_US.pdf).

Carrier ID cards are normally received within 2-3 weeks from when you submit your benefit elections. If you or your medical providers are in need of immediate confirmation of your coverage, please contact the carrier directly.

### Group Medical Coverage

_If you already have current group medical coverage, you may choose to waive or
opt out of TriNet's group health benefits. If you choose to waive health coverage,
you will receive a $300.00 monthly benefit allowance and will still be able to
enroll in optional plans and flexible spending accounts._

#### Medical

TriNet partners with leading carriers, like Aetna, Florida Blue, Blue Shield of
California and Kaiser, to offer a broad range of medical plans, including high
deductible health plans, PPOs, and HMOs. Medical plan options vary by state, and
may also include regional carriers.

##### Obtaining Plan Cost Amounts

1. Go to HR Passport homepage.
1. Select Employee view.
1. Go to Benefits - Resources in the dashboard.
1. Select Plan Costs.
1. Build or maintain custom benefits summary.
1. Select the appropriate location.
1. A report will be generated with all plan costs available in the area.

##### Transgender Medical Services

Recently, the [United States Department of Health and Human Services](https://www.hhs.gov/) released a [mandate on transgender non-discrimination](http://www.transgendermandate.org/). As part of this mandate, medical carriers were given time to review their current policies and update to reflect the mandate. Expanded language for coverage should be visible in the 2017-10-01 renewal for TriNet’s plans.

Because there is a wide range of services, treatment, and goals for transgender patients, employees are encouraged to contact their carrier directly to have these discussions.

#### Pregnancy & Maternity Care

With medical plans, GitLab offers pregnancy and maternity care. Depending on the plan you selected through TriNet, your coverages may differ for in-network vs out-of-network, visits, and inpatient care. Please contact TriNet at +1 800 638 0461 with any questions about your plan.

#### Dental

TriNet's four dental carriers -Delta Dental, Aetna, Guardian Dental, and MetLife-
offer a high and a low national dental PPO plan. Aetna and Delta Dental also
will make available a DMO plan in many states.

#### Vision

TriNet also offers a high and a low vision plan nationally through two different
carriers: Aetna and Vision Service Plan (VSP). These plan options ensure that
you can choose the best plan and carrier for your individual vision needs.

#### Premiums

The Company pays 100% of premiums for medical, 100% of premiums for dental and
100% of premiums for vision coverage for employee. In addition, the Company
funds 50% of premiums for medical, 50% of premiums for dental and 50% of premiums
for vision coverage for spouse, dependent, and domestic partner. These contribution
amounts (monthly) are capped at:

| Insurance (2017-10-01 - 2018-09-30)| Company Contribution Cap |
| ---------------------------------- | -----------------------: |
| Medical Employee Only              |                  $547.00 |
| Medical Employee + Spouse          |                  $905.50 |
| Medical Employee + Child(ren)      |                  $831.50 |
| Medical Employee + Family          |                $1,102.50 |
| Group Dental Employee Only         |                   $24.67 |
| Group Dental Employee + Spouse     |                   $37.63 |
| Group Dental Employee + Child(ren) |                   $38.86 |
| Group Dental Employee + Family     |                   $51.81 |
| Group Vision Employee Only         |                    $9.17 |
| Group Vision Employee + Spouse     |                   $13.78 |
| Group Vision Employee + Child(ren) |                   $14.42 |
| Group Vision Employee + Family     |                   $20.30 |


You are responsible for the remainder of the premium cost, if any. If you do not enroll in a plan within your benefits election period, you will automatically receive the [medical waiver allowance](/handbook/benefits/#group-medical).

### Basic Life Insurance and AD&D

TriNet offers company paid basic life and accidental death and dismemberment (AD&D)
plans. The Company pays for basic life insurance coverage valued at $20,000, which
includes an equal amount of AD&D coverage.

### Short and Long-Term Disability Insurance

Disability insurance plans are designed to provide income protection while you recover
from a disability. This coverage not only ensures that you are able to receive some
income while out on disability; it also provides absence management support that helps
facilitate your return to work. TriNet offers several short and long term disability
plan options. Note: There are five states that have state-mandated disability insurance requirements
California, Hawaii, New Jersey, New York and Rhode Island. If you do not live in one of the listed
states it is recommended that you elect Short Term Disability Insurance through TriNet to protect
your financial situation. For more information on State Disability Insurance please contact People Ops.

#### Group Long-Term and Short-Term Disability Insurance

The Company provides a policy that may replace up to 66.7% of your salary, up to
a maximum benefit of $12,500 per month, for qualifying disabilities. A waiting
period of 180 days will apply.

### 401k Plan

The company offers a 401k plan in which you may make voluntary pre-tax contributions
toward your retirement. We do not currently offer matching contributions. See the
[People Operations](https://about.gitlab.com/handbook/people-operations/) page for
details on eligibility and sign up.

### Employee Assistance Program

The Employee Assistance Program, or [EAP](https://drive.google.com/file/d/0Bwy71gCp1WgtRFg2a1BtNGRsUEU/view?usp=sharing), provides resources to help resolve personal concerns that may be affecting your health, well-being, family life, or job performance.

### Optional TriNet Plans Available at Employee Expense

#### Flexible Spending Account (FSA) Plans

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses
on a pretax basis. You determine your projected expenses for the Plan Year and then
elect to set aside a portion of each paycheck into your FSA.

#### Supplemental Life Insurance

If you want extra protection for yourself and your eligible dependents, you have
the option to elect supplemental life insurance. You may request coverage yourself
for one to six times your annual salary, with a maximum benefit of $2,000,000.
Spousal coverage is also available in increments of $10,000 up to $150,000, and
child coverage for $10,000. Note that amounts above guaranteed issue
($300,000 for you and $30,000 for your spouse) and certain coverage increases
must be approved by the insurance carrier.

#### Supplemental Accidental Death and Dismemberment Insurance

AD&D covers death or dismemberment from an accident only. You may elect supplemental
AD&D coverage in amounts of $25,000, $50,000, $100,000, $250,000, $500,000 or $750,000.
