---
layout: job_page
title: "Developer"
---

At GitLab, developers are highly independent and self-organized individual
contributors who work together as a tight team in a [remote and agile](/2015/09/14/remote-agile-at-gitlab/) way.

Most backend developers work on all aspects of GitLab, building features, fixing bugs, and generally improving the application.
Some developers [specialize](/jobs/specialist) and focus on a specific area, such as packaging, performance or GitLab CI.
Developers can specialize immediately after joining, or after some time, when they have gained familiarity with many areas of GitLab and find one they would like to focus on.

<%= partial "includes/positions/developer/responsibilities" %>

## Requirements

* You can reason about software, algorithms, and performance from a high level
* You are passionate about open source
* You have worked on a production-level Ruby application, preferably using Rails. (This is a [strict requirement](#ruby-experience))
* You know how to write your own Ruby gem using TDD techniques
* Strong written communication skills
* Experience with Docker, Nginx, Go, and Linux system administration a plus
* Experience with online community development a plus
* Self-motivated and have strong organizational skills
* You share our [values](/handbook/values), and work in accordance with those values.
* [A technical interview](/handbook/hiring/interviewing/technical) is part of the hiring process for this position.

### Ruby experience

For this position, a significant amount of experience with Ruby is a **strict requirement**.

We would love to hire all great backend developers, regardless of the language they have most experience with,
but at this point we are looking for developers who can get up and running within the GitLab code base very quickly
and without requiring much training, which limits us to developers with a large amount of existing experience with Ruby, and preferably Rails too.

For a time, we also considered applicants with little or no Ruby and Rails experience for this position,
because we realize that programming skills are to a large extent transferable between programming languages,
but we are not currently doing that anymore for the reasons described in the
[merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2695)
that removed the section from this listing that described that policy.

If you think you would be an asset to our engineering team regardless, please see if [another position](/jobs) better fits your experiences and interests,
or apply using the [Open Application](/jobs/open-application/).

If you would still prefer to join the backend development team as a Ruby developer,
please consider contributing to the open-source [GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce).
We frequently hire people from the community who have shown through contributions that
they have the skills that we are looking for, even if they didn’t have much previous experience
with those technologies, and we would gladly review those contributions.

## Junior Developers

Junior Developers share the same requirements outlined above, but typically
join with less or alternate experience in one of the key areas of Developer
expertise (Ruby on Rails, Git, reviewing code). For example,
a person with extensive experience in a web framework other than RoR, but with experience
on the other areas would typically join as a Junior.

<%= partial "includes/senior_developer" %>

## Staff Developers

A Senior Developer will be promoted to a Staff Developer when they have
demonstrated significant leadership to deliver high-impact projects. This may
involve any type of consistent "above and beyond senior level" performance,
for example:

1. Technical Skills
    * Can demonstrate a deep knowledge and experience with multiple technical domains
      as well as a very good broad knowledge of the entire GitLab technology stack.
      What Kent Beck refers to as a
      [Paint Drip Person](https://www.facebook.com/notes/kent-beck/paint-drip-people/1226700000696195)
    * Proactively identifies and reduces technical debt
    * Identifies and reduces scalability issues and performance bottlenecks
      in the GitLab stack
2. Leadership
    * Actively contributes to the long-term strategic architectural vision for
      the company, helping to set the technical direction for GitLab
    * Works across functional groups, engaging stakeholders, gatekeepers, and
      engineers to deliver major features from idea to production
    * Demonstrates the ability to break a complex problem down into a set of
      [minimum viable changes](http://conversationaldevelopment.com/shorten-cycle/)
      that can be shipped through a series of short release cycles
3. Communication
    * Proactively shares knowledge and radiates GitLab technical skills to team
      members through mentorship, documentation, pairing and other approaches
    * Excellent written and spoken language skills: a staff developer should be
      able to communicate their ideas
      [efficiently](https://about.gitlab.com/handbook/values/#efficiency) and concisely

## Internships

We normally don't offer any internships, but if you get a couple of merge requests
accepted, we'll interview you for one. This will be a remote internship without
supervision; you'll only get feedback on your merge requests. If you want to
work on open source and qualify please submit an application.
In the cover letter field, please note that you want an internship and link to
the accepted merge requests. The merge requests should be of significant
value and difficulty, which is at the discretion of the manager. For
example, fixing 10 typos isn't as valuable as shipping 2 new features.

## Workflow

The basics of GitLab development can be found in the [developer onboarding](/handbook/developer-onboarding/#basics-of-gitlab-development) document.

The handbook details the complete [GitLab Workflow](/handbook/communication/#gitlab-workflow).

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
