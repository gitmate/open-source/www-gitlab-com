---
layout: job_page
title: "Data analyst lead"
---

## Responsibilities

### Goal

- Effectively focus our efforts in product, marketing, sales, and customer success.
- Implement our [analytics effort](https://about.gitlab.com/handbook/finance/analytics).

### Reporting

You will report to the CFO. You'll work with Product, Marketing, and Sales leadership and operations people to coordinate the central data model.

### Requirements

TODO
