/* global jQuery */
(function($) {
  'use strict';

  var LifecycleDiagram = {
    _diagram: [],
    _phases: [],
    slicked: false,
    _body: [],
    _diagram_nav: [],
    init: function() {
      var self = LifecycleDiagram;
      self._diagram = $('.home-lifecycle-diagram');
      if ( self._diagram.length > 0 ) {
        self.setup();
      }
    },
    setup: function() {
      var self = this;
      self._phases = $('.lifecycle-diagram-phase');
      self._body = $('body');
      self.setupNavigation();
      self.checkCarousel();
      self.checkHeights();
      $(document).on('mouseover', '.lifecycle-diagram-navigation li', self.onMouseover);
      $(window).on('resize', function() { self.checkCarousel(); self.checkHeights(); });
      $(window).on('load', function() { self.checkHeights(); });
    },
    setupNavigation: function() {
      var self = this
        , $ul = $('<ul class="lifecycle-diagram-navigation"/>')
      ;

      self._phases.each(function() {
        var $$ = $(this)
          , title = $$.data('title')
          , $icon = $$.find('svg:first').remove()
          , $li = $('<li/>')
        ;

        $li.data('content', $$);
        $li.append( $icon, $('<span>' + title + '</span>') );
        $ul.append($li);
      });

      self._diagram.prepend($ul);
      $ul.children().first().addClass('is-active');
      self._diagram_nav = $ul;
    },
    onMouseover: function() {
      LifecycleDiagram.activatePhase($(this));
    },
    activatePhase: function($phase) {
      $phase
        .addClass('is-active')
        .siblings()
          .removeClass('is-active')
        .end()
        .data('content')
          .show()
          .siblings('.lifecycle-diagram-phase:visible')
            .hide()
      ;
    },
    checkCarousel: function() {
      var self = this
        , slick_args = { variableWidth: true, infinite: false }
      ;

      if ( self._body.width() < 768 ) {
        if ( !self.slicked ) {
          self._diagram_nav.slick(slick_args);
          self.slicked = true;
        }
      } else {
        if ( self.slicked ) {
          self._diagram_nav.slick('unslick');
          self.slicked = false;
        }
      }
    },
    checkHeights: function() {
      var self = this
        , min_height = 0
      ;

      self._phases.css( 'min-height', '');
      self._diagram.addClass('is-checking-height');
      self._phases.each(function() {
        var this_height = $(this).height();
        if ( this_height > min_height ) {
          min_height = this_height;
        }
      });
      self._diagram.removeClass('is-checking-height');
      self._phases.css('min-height', min_height + 'px');
    }
  };
  $(LifecycleDiagram.init);

  // Support for tiles or other content that is entirely clickable
  var AllClickable = {
    init: function() {
      var self = AllClickable;

      $(document).on({
        mouseenter: self.mouseenter,
        mouseleave: self.mouseleave
      }, '.js-all-clickable');
      $('.js-all-clickable').click(self.click);
    },
    mouseenter: function() {
      $(this).addClass('js-hover');
    },
    mouseleave: function() {
      $(this).removeClass('js-hover');
    },
    click: function(event) {
      var $$ = $(this)
        , link = $$.find('.js-all-clickable-click-target').get(0)
      ;

      $$.parents().blur();
      if ( !link ) {
        link = $$.find('a:first').get(0);
      }
      if (link && event.target !== link) {
        link.click();
      }
    }
  };
  $(AllClickable.init);
})(jQuery);
