---
layout: markdown_page
title: "GitLab Strategy"
---

## Why

GitLab was created because Dmitriy needed an affordable tool to collaborate with his team. He wanted something efficient and enjoyable so he could focus on his work, not the tools. He worked on it from home, a house in Ukraine without running water.

We think that it is logical that our collaboration tools are a collaborative work themselves. More than [1000 people](http://contributors.gitlab.com/) have contributed to GitLab to make that a reality. We believe in a world where **everyone can contribute**. Allowing everyone to make a proposal is the core of what a DVCS ([Distributed Version Control System](https://en.wikipedia.org/wiki/Distributed_version_control)) such as Git enables. No invite needed: if you can see it, you can contribute.


In summary, our vision is as follows:

We believe that all digital products should be open to contributions, from legal documents to movie scripts and from websites to chip designs. GitLab Inc. develops great open source software to enable people to collaborate in this way. GitLab is integrated and opinionated collaboration software that everyone should be able to afford and adapt. With GitLab, **everyone can contribute**.

## Mission

Change all creative work from read-only to read-write so that **everyone can contribute**.

## How

Everyone can contribute to digital products with GitLab, to GitLab itself, and to our organization.

1. To ensure that **everyone can contribute to digital products** we make GitLab joyful to use. It is integrated and opinionated collaboration software. Because there is no need to string together multiple tools you spend less time, have less frustration, there is less [bikeshedding](https://en.wikipedia.org/wiki/Law_of_triviality), and you get more results.

2. To ensure that **everyone can contribute with GitLab** every person in the world should be able to afford it and adapt it. GitLab CE and GitLab.com are both [free as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/) so everyone can afford it. GitLab.com comes with free private repos and CI runners, so you don't even have to host it yourself. People should be able to adapt GitLab to fit their needs. That is why GitLab CE is also [free as in speech](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/); we distribute the source and use [MIT Expat license](https://www.debian.org/legal/licenses/mit). But open source is more than a license, that is why we actively help our competitor Perforce to ship GitLab as part of Perforce Helix, and are [a good steward of GitLab CE](https://about.gitlab.com/about/#stewardship). And we keep all our software open to inspection, modifications, enhancements, and suggestions.

3. To ensure that **everyone can contribute to GitLab itself** we actively welcome contributors. We do this by having quality code, tests, documentation, using popular frameworks, offering a comprehensive [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit), and a dedicated [GitLab Design Kit](https://gitlab.com/gitlab-org/gitlab-design). We use GitLab at GitLab Inc., to drink our own wine and make it a tool we continue to love. We celebrate contributions by recognizing a Most Valuable Person (MVP) every month. We allow everyone to anticipate, propose, discuss, and contribute features by having everything on a public issue tracker. We ship a new version every month so contributions and feedback are visible fast. To contribute to open source software people must be empowered to learn programming. That is why we sponsor initiatives such as Rails Girls and [Lean Poker](http://leanpoker.org).

4. To ensure that **everyone can contribute to our organization** we have open business processes that allow all team members to suggest improvements to our handbook. We hire remotely so everyone with an internet connection can come work for us and be judged on results, not presence in the office. We offer equal opportunity for every nationality. We are agnostic to location and create more equality of opportunity in the world. We engage on Hacker News, Twitter, and our blog post comments. And we strive to take decisions guided by [our values](https://about.gitlab.com/handbook/values).

## Goals

1. Ensure that **everyone can contribute** in the 4 ways outlined above.

2. Become most used software for the software development lifecycle and collaboration on all digital content by following [the sequence below](#sequence).

3. Complete our product vision of an [opinionated and integrated application based on convention over configuration that offers superior user experience](https://about.gitlab.com/direction/#vision).

4. Offer a sense of progress [in a supportive environment with smart colleagues](http://pandodaily.com/2012/08/10/dear-startup-genius-choosing-co-founders-burning-out-employees-and-lean-vs-fat-startups/).

5. Stay independent so we can preserve our values. Since we took external investment we need a [liquidity event](https://en.wikipedia.org/wiki/Liquidity_event). To stay independent we want that to be an IPO instead of being acquired.

## Sequence <a name="sequence"></a>

We want to achieve our goals in the following order:

1. In 2015 we became the most popular on-premises software development lifecycle solution, and we want to continue that.

2. We want to become the most revenue generating on-premises software development lifecycle solution before our IPO.

3. Around our IPO we want to become the most popular SaaS solution for private repositories (a [complete product](https://about.gitlab.com/direction/#scope) that is [free forever](https://about.gitlab.com/gitlab-com/#why-gitlab-com-will-be-free-forever) is competitive since network effects are smaller for private repositories than for public ones).

4. After our IPO we want to become the most popular SaaS solution for public repositories. This market has a [strong network effect](https://en.wikipedia.org/wiki/Network_effect) since more people will participate if you host your public project on a site with more people. It is easier to overcome this network effect if many people already use GitLab.com for hosting private repositories. Having people on our SaaS helps drive awareness and familiarity with GitLab.

5. Our [BHAG](https://en.wikipedia.org/wiki/Big_Hairy_Audacious_Goal) is to become the most popular collaboration tool for knowledge workers in any industry. For this, we need to make the git workflow much more user friendly. The great thing is that sites like [Penflip](https://www.penflip.com/) are already building on GitLab to make it.

We want to IPO in 2020, this is five years after the first people got stock options with 4 years of vesting. To IPO we need more than $100m in revenue. To achieve that we want to triple ARR (Annual Recurring Revenue) in 2016&17 (more than achieved that in 2016), and double it in 2018&19&20 ([T2D3](https://techcrunch.com/2015/02/01/the-saas-travel-adventure/)).

While we achieve our goals one by one, this doesn't mean we will focus on only one goal at a time. Simultaneously, we'll grow our userbase, get more subscribers for [EE](https://about.gitlab.com/features/#enterprise), grow [GitLab.com](https://about.gitlab.com/gitlab-com/), develop [products](https://about.gitlab.com/pricing/#gitlab-ee), realize our [scope](https://about.gitlab.com/direction/#scope), and make version control usable for more types of work.

During phase 2 there is a natural inclination to focus only on on-premises since we make all our money there. Having GitHub focus on SaaS instead of on-premises gave us a great opportunity to achieve phase 1. But GitHub was not wrong, they were early. When everyone was focused on video on demand Netflix focused on shipping DVD's by mail. Not because it was the future but because it was the biggest market. The biggest mistake they could have made was to stick with DVDs. Instead they leveraged the revenue generated with the DVDs to build the best video on demand service.

We realize our competitors have started earlier and have more capital. Because we started later we need a more compelling product that covers the complete [scope](https://about.gitlab.com/direction/#scope), that is integrated out of the box, and that is cloud native. Because we have less capital, we need to build that as a community. Therefore it is important to share and ship our [vision for the product](https://about.gitlab.com/direction/#vision). The people that have the most knowledge have to prioritize **breadth over depth** since only they can add new functionality. Making the functionality more comprehensive requires less coordination than making the initial minimal feature. Shipping functionality that is incomplete to expand the scope sometimes goes against our instincts. However leading the way is needed to allow others to see our path and contribute. With others contributing, we'll iterate faster to [improve and polish functionality over time](http://mark.pundsack.com/2017/03/14/Polygonal-Product-Management/). So when in doubt, the rule of thumb is breadth over depth, so everyone can contribute.

## Principles

1. Founder control: vote & board majority so we can keep making long term decisions.

2. Independence: since we took financing we need to have a [liquidity event](https://en.wikipedia.org/wiki/Liquidity_event); to maintain independence this needs to be an IPO rather than an acquisition.

3. Low burn: spend seed money like it is the last we’ll raise, maintain 2 years of runway.

4. First time right: last to market so we get it right the first time, a fast follower with taste.

5. Values: make decisions based on [our values](https://about.gitlab.com/handbook/values), even if it is inconvenient.

6. Free SaaS: to make GitLab.com the most popular SaaS for private projects in 2020 it should not have limits for projects or collaborators.

7. Reach: go for a broad reach, no focus on business verticals or certain programming languages.

8. Speed: ship every change in the next release to maximize responsiveness and learning.

9. Life balance: we want people to stay with us for a long time, so it is important to take time off, work on life balance, and being remote-only is a large part of the solution.

## Assumptions

1. [Open source user benefits](http://buytaert.net/acquia-retrospective-2015): significant advantages over proprietary software because of its faster innovation, higher quality, freedom from vendor lock-in, greater security, and lower total cost of ownership.

2. [Open Source stewardship](https://about.gitlab.com/about/#stewardship): community comes first, we [plays well with others](https://about.gitlab.com/direction/#external-integrations) and share the pie with competitors.

3. [Innersourcing](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/) is needed and will force companies to choose one solution top-down.

4. Git will dominate the version control market in 2020.

5. An [integrated tool](https://medium.com/@gerstenzang/developer-tools-why-it-s-hard-to-build-a-big-business-423436993f1c#.ie38a0cls) is superior to a collection of tools or a network of tools. Even so, good integrations are important for network effects and making it possible to integrate GitLab into an organization.

6. To be sustainable we need an open core model that includes a proprietary GitLab EE.

7. EE needs a low base price that is publicly available to compete for reach with CE, established competitors, and new entrants to the market.

8. The low base price for EE is supplemented by a large set of options aimed at larger organizations that get a lot of value from GitLab.

## Pricing

Most of GitLab functionality is and will be available for free in Community Edition (CE). Our paid Enterprise Edition (EE) includes features that are [more relevant for organizations that have more than 100 potential users](https://about.gitlab.com/about/#what-features-are-ee-only). As [we promise](https://about.gitlab.com/about/#promises) all major features in [our scope](https://about.gitlab.com/direction/#scope) are available in GitLab CE too. Instead of charging for specific parts of our scope (CI, Monitoring, etc.) we charge for smaller features that you are more likely to need if you use GitLab with a lot of users. There are a couple of reasons for this:

1. We want to be a good [steward of our open source product](https://about.gitlab.com/about/#stewardship).
1. Giving a great free product is part of our go to market, it helps create new users and customers.
1. Having our scope available to all users increases adoption of our scope and helps people see the benefit of an [integrated toolset](https://about.gitlab.com/direction/#integrated-toolset).
1. Including all major features in CE helps reduce merge conflicts between CE and EE

Because we have a great free product we can't have one price.
Setting it high would make the difference from the free version too high.
Setting it low would make it hard to run a sustainable business.
There is no middle ground that would work out with one price.

That is why we have a [starter and a premium edition of GitLab EE](https://about.gitlab.com/handbook/product/#enterprise-edition-tiers).
In the future we might also introduce an ultimate edition.
The price difference between each of them is half an order of magnitude (5x).
When you use them on GitLab.com we charge slightly more to compensate for our hosting costs.

We charge for making people more effective and will charge per user, per application, or per instance.
We do include free minutes with our subscriptions and trials to make it easier for users to get started.
As we look towards more deployment related functionality on .com it's tempting to offer compute and charge a percent on top of for example Google Cloud Platform (GCP).
We don't want to charge an ambiguous margin on top of another provider since this limits user choice and is not transparent.
So we will always let you BYOK (bring your own Kubernetes) and never lock you into our infrastructure to charge you an opaque premium on those costs.

## Quarterly Objectives and Key Results (OKRs)

To make sure our goals are clearly defined and aligned throughout the organization, we make use of OKR's (Objective Key Results). Our [quarterly Objectives and Key Results](/okrs) are publicly viewable.

## Why is this page public?

Our strategy is completely public because transparency is one of our [values](/handbook/values). We're not afraid of sharing our strategy because as Peter Drucker said: "Strategy is a commodity, execution is an art.".
